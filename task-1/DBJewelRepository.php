<?php

namespace application\impl\repos;

use application\domain\repos\JewelRepository;
use common\models\DBJewel;
use common\models\DBJewelMaterial;
use yii\base\Exception;
use application\domain\entities\Jewel;
use application\domain\entities\Material;
use application\domain\services\JewelService;
use application\domain\entities\Workshop;
use application\domain\factories\JewelFactory;
use application\domain\repos\CategoryRepository;
use application\domain\repos\CityRepository;
use application\domain\repos\WorkshopRepository;
use application\domain\repos\StyleRepository;
use application\domain\repos\MaterialRepository;
use application\domain\repos\PriceRangeRepository;

/**
 * Репозиторий изделий с хранением в БД.
 * @package application\repos
 */
class DBJewelRepository extends JewelRepository
{
    /**
     * @inheritdoc
     */
    public function findOne($id)
    {
        /* @var DBJewel $AR */
        $AR = DBJewel::find()->where('id = :id', [':id' => $id])->one();
        if (empty($AR)) {
            return null;
        }

        return $this->build($AR);
    }
    /**
     * @inheritdoc
     */
    public function findByIds(array $ids = [])
    {
        $ARs = DBJewel::find()->where('id IN ('.join(',', $ids).')')->all();

        $result = [];
        foreach ($ARs as $AR) {
            /* @var DBJewel $AR */
            $Model = $this->build($AR);
            $result[] = $Model;
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function findAll($offset = null, $limit = null)
    {
        $Query = DBJewel::find();

        // офсет / лимит
        if (!is_null($offset)) {
            $Query->offset($offset);
        }
        if (!is_null($limit)) {
            $Query->limit($limit);
        }

        $ARs = $Query->all();

        $result = [];
        foreach ($ARs as $AR) {
            /* @var DBJewel $AR */
            $Model = $this->build($AR);
            $result[] = $Model;
        }

        return $result;
    }
    /**
     * @inheritdoc
     */
    public function findByWorkshop(Workshop $Workshop, $offset = null, $limit = null)
    {
        $Query = DBJewel::find();
        $Query->andWhere('id_workshop = :id', [':id' => $Workshop->getId()]);

        // офсет / лимит
        if (!is_null($offset)) {
            $Query->offset($offset);
        }
        if (!is_null($limit)) {
            $Query->limit($limit);
        }

        $ARs = $Query->all();

        $result = [];
        foreach ($ARs as $AR) {
            /* @var DBJewel $AR */
            $Model = $this->build($AR);
            $result[] = $Model;
        }

        return $result;
    }
    /**
     * @inheritdoc
     */
    public function findBy(array $criteria = [], $offset = null, $limit = null)
    {
        $Query = $this->prepareQuery($criteria, $offset, $limit);

        $ARs = $Query->all();

        $result = [];
        foreach ($ARs as $AR) {
            /* @var DBJewel $AR */
            $Model = $this->build($AR);
            $result[] = $Model;
        }

        return $result;
    }
    /**
     * @inheritdoc
     */
    public function countBy(array $criteria = [])
    {
        $Query = $this->prepareQuery($criteria);
        $Query->orderBy = null; // count(*) не может быть вместе с order by без группировки

        $result = $Query->count();

        return $result;
    }
    /**
     * @inheritdoc
     */
    public function save(Jewel $Jewel)
    {
        $id = $Jewel->getId();

        $AR = empty($id) ? new DBJewel() : DBJewel::find()->where('id = :id', [':id' => $id])->one();

        $AR->name = $Jewel->getName();
        $AR->description = $Jewel->getDescription();
        $AR->price_numeric = $Jewel->getPriceNumeric();
        $AR->price_text = $Jewel->getPriceText();
        $AR->terms = $Jewel->getTerms();
        $AR->size = $Jewel->getSize();
        $AR->weight = $Jewel->getWeight();

        $AR->on_demand = $Jewel->getOnDemand();
        $AR->on_demand_comment = $Jewel->getOnDemandComment();

        $AR->gender = $Jewel->getGender();

        $AR->gift_pack = $Jewel->getGiftPack();
        $AR->free_shipping = $Jewel->getFreeShipping();

        $AR->hide = $Jewel->getHide();

        $AR->photo1_hash = $Jewel->getPhoto1Hash();
        $AR->photo2_hash = $Jewel->getPhoto2Hash();
        $AR->photo3_hash = $Jewel->getPhoto3Hash();
        $AR->photo4_hash = $Jewel->getPhoto4Hash();
        $AR->photo1_extension = $Jewel->getPhoto1Extension();
        $AR->photo2_extension = $Jewel->getPhoto2Extension();
        $AR->photo3_extension = $Jewel->getPhoto3Extension();
        $AR->photo4_extension = $Jewel->getPhoto4Extension();

        $AR->created_at = $Jewel->getCreatedAt();
        $AR->updated_at = $Jewel->getUpdatedAt();

        $AR->id_category = $Jewel->getCategory()->getId();
        $AR->id_city = $Jewel->getCity()->getId();
        $AR->id_workshop = $Jewel->getWorkshop()->getId();
        $AR->id_style = $Jewel->getStyle()->getId();
        $AR->id_price_range = $Jewel->getPriceRange()->getId();

        $Transaction = DBJewel::getDb()->beginTransaction();
        try {
            $saved = $AR->save();
            if (!$saved) { throw new Exception(); }

            $idJewel = $AR->id;
            $materials = $Jewel->getMaterials();
            $idsMaterials = [];
            foreach ($materials as $Material) {
                /* @var Material $Material */
                $idsMaterials[] = $Material->getId();
            }

            DBJewelMaterial::deleteAll('id_jewel = :id', [':id' => $idJewel]);

            if (!empty($idsMaterials)) {
                foreach ($idsMaterials as $idMaterial) {
                    $ARJunction = new DBJewelMaterial();
                    $ARJunction->id_jewel = $idJewel;
                    $ARJunction->id_material = $idMaterial;
                    $ARJunction->created_at = $Jewel->getCreatedAt();
                    $ARJunction->updated_at = $Jewel->getUpdatedAt();
                    $saved = $ARJunction->save();
                    if (!$saved) { throw new Exception(); }
                }
            }

            $Transaction->commit();
        } catch(\Exception $E) {
            $Transaction->rollBack();
            throw $E;
        }

        return $AR->id;
    }
    /**
     * @inheritdoc
     */
    public function delete(Jewel $Jewel)
    {
        $Transaction = DBJewel::getDb()->beginTransaction();
        try {

            DBJewel::deleteAll('id = :id', [':id' => $Jewel->getId()]);
            DBJewelMaterial::deleteAll('id_jewel = :id', [':id' => $Jewel->getId()]);

            $Transaction->commit();
        } catch(\Exception $E) {
            $Transaction->rollBack();
            throw $E;
        }
    }
    /**
     * @inheritdoc
     */
    public function getIds(/*$includingHidden = true*/)
    {
        $ARs = DBJewel::find()
            ->select('id')
            //->where('hide = :hide', [':hide' => $includingHidden])
            ->orderBy('id ASC')
            ->all();

        $result = [];
        foreach ($ARs as $AR) {
            /* @var DBJewel $AR */
            $result[] = $AR->id;
        }
        return $result;
    }
    /**
     * @inheritdoc
     */
    public function getIdsAndNames(/*$includingHidden = true*/)
    {
        $ARs = DBJewel::find()
            ->select('id, name')
            //->where('hide = :hide', [':hide' => $includingHidden])
            ->orderBy('id ASC')
            ->all();
        $result = [];
        foreach ($ARs as $AR) {
            /* @var DBJewel $AR */
            $result[$AR->id] = $AR->name;
        }
        return $result;
    }
    /**
     * @param DBJewel $AR
     * @return Jewel
     */
    protected function build(DBJewel $AR)
    {
        $Jewel = $this->ServiceLocator->getJewelFactory()->create();

        $Jewel->setId($AR->id);

        $Jewel->setName($AR->name);
        $Jewel->setDescription($AR->description);
        $Jewel->setPriceNumeric($AR->price_numeric);
        $Jewel->setPriceText($AR->price_text);
        $Jewel->setTerms($AR->terms);
        $Jewel->setSize($AR->size);
        $Jewel->setWeight($AR->weight);

        $Jewel->setOnDemand($AR->on_demand);
        $Jewel->setOnDemandComment($AR->on_demand_comment);

        $Jewel->setGender($AR->gender);

        $Jewel->setGiftPack($AR->gift_pack);
        $Jewel->setFreeShipping($AR->free_shipping);

        $Jewel->setHide($AR->hide);

        $Jewel->setPhoto1Hash($AR->photo1_hash);
        $Jewel->setPhoto2Hash($AR->photo2_hash);
        $Jewel->setPhoto3Hash($AR->photo3_hash);
        $Jewel->setPhoto4Hash($AR->photo4_hash);
        $Jewel->setPhoto1Extension($AR->photo1_extension);
        $Jewel->setPhoto2Extension($AR->photo2_extension);
        $Jewel->setPhoto3Extension($AR->photo3_extension);
        $Jewel->setPhoto4Extension($AR->photo4_extension);

        $Jewel->setCreatedAt($AR->created_at);
        $Jewel->setUpdatedAt($AR->updated_at);

        $Jewel->setCategory($this->ServiceLocator->getCategoryRepository()->findOne($AR->id_category));
        $Jewel->setCity($this->ServiceLocator->getCityRepository()->findOne($AR->id_city));
        $Jewel->setWorkshop($this->ServiceLocator->getWorkshopRepository()->findOne($AR->id_workshop));
        $Jewel->setStyle($this->ServiceLocator->getStyleRepository()->findOne($AR->id_style));
        $Jewel->setPriceRange($this->ServiceLocator->getPriceRangeRepository()->findOne($AR->id_price_range));

        $ARsJunction = DBJewelMaterial::find()->where('id_jewel = :id', [':id' => $AR->id])->all();
        $materials = [];
        foreach ($ARsJunction as $ARJunction) {
            /* @var DBJewelMaterial $ARJunction */
            $materials[] = $this->ServiceLocator->getMaterialRepository()->findOne($ARJunction->id_material);
        }
        $Jewel->setMaterials($materials);

        return $Jewel;
    }
    /**
     * @param array $criteria
     * @param int $offset
     * @param int $limit
     * @return \yii\db\ActiveQuery
     */
    protected function prepareQuery(array $criteria = [], $offset = null, $limit = null)
    {
        $Query = DBJewel::find();

        // фильтры
        $categoryCriteria = self::CRITERIA_CATEGORY;
        if (array_key_exists($categoryCriteria, $criteria)) {
            $Query->andWhere('id_category = :id_category', [':id_category' => $criteria[$categoryCriteria]]);
        }
        $sexCriteria = self::CRITERIA_SEX;
        if (array_key_exists($sexCriteria, $criteria)) {
            $Query->andWhere('gender = :gender OR gender = '.self::VALUE_SEX_BOTH, [':gender' => $criteria[$sexCriteria]]);
        }
        $priceRangeCriteria = self::CRITERIA_PRICE_RANGE;
        if (array_key_exists($priceRangeCriteria, $criteria)) {
            $Query->andWhere('id_price_range = :id_price_range', [':id_price_range' => $criteria[$priceRangeCriteria]]);
        }
        $styleCriteria = self::CRITERIA_STYLE;
        if (array_key_exists($styleCriteria, $criteria)) {
            $Query->andWhere('id_style = :id_style', [':id_style' => $criteria[$styleCriteria]]);
        }
        $cityCriteria = self::CRITERIA_CITY;
        if (array_key_exists($cityCriteria, $criteria)) {
            $Query->andWhere('id_city = :id_city', [':id_city' => $criteria[$cityCriteria]]);
        }
        $workshopCriteria = self::CRITERIA_WORKSHOP;
        if (array_key_exists($workshopCriteria, $criteria)) {
            $Query->andWhere('id_workshop = :id_workshop', [':id_workshop' => $criteria[$workshopCriteria]]);
        }
        $exceptWorkshopCriteria = self::CRITERIA_EXCEPT_WORKSHOP;
        if (array_key_exists($exceptWorkshopCriteria, $criteria)) {
            $Query->andWhere('id_workshop != :id_workshop', [':id_workshop' => $criteria[$exceptWorkshopCriteria]]);
        }

        // отбрасываем скрытые
        $includingHiddenCriteria = self::CRITERIA_INCLUDING_HIDDEN;
        if (!array_key_exists($includingHiddenCriteria, $criteria) || $criteria[$includingHiddenCriteria] != true) {
            $Query->andWhere('hide = :hide', [':hide' => false]);
        }

        // случайная выборка
        $randomCriteria = self::CRITERIA_RANDOM;
        if (array_key_exists($randomCriteria, $criteria)) {
            if ($criteria[$randomCriteria] == true) {
                // TODO refactoring
                $Query->addOrderBy('random()'); // так годится только для postgresql
            }
        }

        // сортировка
        $sortCriteria = self::CRITERIA_SORT;
        if (array_key_exists($sortCriteria, $criteria)) {
            if ($criteria[$sortCriteria] == self::VALUE_SORT_PRICE) {
                $Query->addOrderBy('price_numeric ASC');
            }
            if ($criteria[$sortCriteria] == self::VALUE_SORT_NOVELTY) {
                $Query->addOrderBy('created_at DESC');
            }
        }

        // офсет / лимит
        if (!is_null($offset)) {
            $Query->offset($offset);
        }
        if (!is_null($limit)) {
            $Query->limit($limit);
        }

        return $Query;
    }

}