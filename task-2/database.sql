CREATE TABLE products ( /* товары */
  id BIGINT PRIMARY KEY,
  name CHARACTER VARYING(255) NOT NULL, /* название */
  code CHARACTER VARYING(255) NOT NULL, /* артикул */
  description TEXT NOT NULL, /* описание */
  characteristics TEXT NOT NULL, /* характеристики */
  price INTEGER NOT NULL, /* цена хранится в копейках */
  quantity INTEGER NOT NULL, /* доступное количество */
  created_at INTEGER NOT NULL, /* таймштамп времени создания записи */
  updated_at INTEGER NOT NULL /* таймштамп времени изменения записи */
);
CREATE TABLE categories ( /* категории товаров */
  id BIGINT PRIMARY KEY,
  category_parent_id BIGINT, /* айди родительской категории */
  name CHARACTER VARYING(255) NOT NULL, /* название */
  created_at INTEGER NOT NULL, /* таймштамп времени создания записи */
  updated_at INTEGER NOT NULL /* таймштамп времени изменения записи */
);
CREATE TABLE products_categories ( /* связь товаров и категорий */
  product_id BIGINT,
  category_id BIGINT,
  PRIMARY KEY (product_id, category_id)
);
CREATE TABLE products_concomitants ( /* сопутствующие товары */
  product_main_id BIGINT,
  product_concomitant_id BIGINT,
  PRIMARY KEY (product_main_id, product_concomitant_id)
);
CREATE TABLE customers ( /* покупатели */
  id BIGINT PRIMARY KEY,
  name CHARACTER VARYING(255) NOT NULL, /* имя */
  /* прочие данные */
  created_at INTEGER NOT NULL, /* таймштамп времени создания записи */
  updated_at INTEGER NOT NULL /* таймштамп времени изменения записи */
);
CREATE TABLE orders ( /* заказы */
  id BIGINT PRIMARY KEY,
  customer_id INTEGER NOT NULL,
  /* прочие данные */
  created_at INTEGER NOT NULL, /* таймштамп времени создания записи */
  updated_at INTEGER NOT NULL /* таймштамп времени изменения записи */
);
CREATE TABLE order_details ( /* детали заказов */
  order_id BIGINT,
  product_id BIGINT,
  quantity INTEGER NOT NULL, /* кол-во */
  price INTEGER NOT NULL, /* цена за единицу, в копейках */
  total INTEGER NOT NULL, /* общая сумма, в копейках */
  /* прочие данные */
  created_at INTEGER NOT NULL, /* таймштамп времени создания записи */
  updated_at INTEGER NOT NULL /* таймштамп времени изменения записи */,
  PRIMARY KEY (order_id, product_id)
);
CREATE TABLE articles ( /* статьи */
  id BIGINT PRIMARY KEY,
  name CHARACTER VARYING(255) NOT NULL, /* название */
  text TEXT NOT NULL, /* текст */
  /* прочие данные */
  created_at INTEGER NOT NULL, /* таймштамп времени создания записи */
  updated_at INTEGER NOT NULL /* таймштамп времени изменения записи */
);
CREATE TABLE products_articles ( /* связь статей и товаров */
  product_id BIGINT,
  article_id BIGINT,
  PRIMARY KEY (product_id, article_id)
);
CREATE TABLE categories_articles ( /* связь статей и категорий */
  category_id BIGINT,
  article_id BIGINT,
  PRIMARY KEY (category_id, article_id)
);